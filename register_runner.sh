#!/bin/sh

CONTAINER="${CONTAINER:-gitlab-runner}"
RUNNER_NAME="${RUNNER_NAME:-${2}}"
REGISTRATION_TOKEN="${REGISTRATION_TOKEN:-${1}}"
DOCKER_BIN="${DOCKER_BIN:-docker}"

ARMv7_MAGIC="7f454c4601010100000000000000000002002800"

CONCURRENT_JOBS="${CONCURRENT_JOBS:-$(($(getconf _NPROCESSORS_ONLN) - 1))}"
if  [ "${CONCURRENT_JOBS}" -lt 1 ]; then                                                 
    CONCURRENT_JOBS=1                                                                    
fi

if [ -z "${REGISTRATION_TOKEN}" ]; then
    echo "Missing registration token, please get one from the CI/CD page."
    exit 1
fi

if [ -z "${RUNNER_NAME}" ]; then
    echo "Missing runner name, please supply one."
    exit 1
fi

for emu in /proc/sys/fs/binfmt_misc/*; do
    if [ ! -r "${emu}" ]; then
        continue
    fi

    if grep -q "${ARMv7_MAGIC}" "${emu}"; then
        ARM_EMU_BIN="$(sed 's/interpreter //;t;d' "${emu}")"
        break
    fi
done

if [ ! -x "${ARM_EMU_BIN}" ]; then
    echo "Missing ARMv7 interpreter, please set ARM_EMU_BIN to a valid interpreter."
    exit 1
fi

if ! command -v "${DOCKER_BIN}" 1> /dev/null; then
    echo "Docker missing, please install docker."
    exit 1
fi

if [ ! -e "/var/lib/docker/data/${CONTAINER}/config.toml" ]; then
    mkdir -p "/var/lib/docker/data/${CONTAINER}"
    cat > "/var/lib/docker/data/${CONTAINER}/config.toml" <<-EOT
	concurrent=${CONCURRENT_JOBS}
EOT
fi

if ! docker inspect "${CONTAINER}" 1> /dev/null 2>&1; then
    docker run \
        -d \
        --name "${CONTAINER}" \
        --restart always \
        -e "ARM_EMU_BIN=${ARM_EMU_BIN}" \
	-v "/var/run/docker.sock:/var/run/docker.sock" \
        -v "${ARM_EMU_BIN}:${ARM_EMU_BIN}:ro" \
        -v "/var/lib/docker/data/${CONTAINER}:/etc/gitlab-runner" \
        registry.gitlab.com/gitlab-org/gitlab-runner:latest
fi

if [ "$(docker inspect -f '{{ .State.Running }}' "${CONTAINER}")" = "false" ]; then
    docker start "${CONTAINER}"
fi

docker exec -it "${CONTAINER}" gitlab-runner register \
    --docker-image "registry.hub.docker.com/library/alpine:latest" \
    --docker-privileged \
    --docker-pull-policy="if-not-present" \
    --docker-tlsverify=true \
    --docker-volume-driver "btrfs" \
    --docker-volumes "${ARM_EMU_BIN}:${ARM_EMU_BIN}:ro" \
    --docker-volumes "/run/docker.sock:/run/docker.sock" \
    --env "ARM_EMU_BIN=${ARM_EMU_BIN}" \
    --executor "docker" \
    --name "${RUNNER_NAME}" \
    --non-interactive \
    --registration-token "${REGISTRATION_TOKEN}" \
    --tag-list "docker,linux,qemu" \
    --url "https://gitlab.com"
