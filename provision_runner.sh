#!/bin/sh
# After manually installing a minimal debian stretch run the following

apt-get install apt-transport-https qemu-user-static

wget --no-check-certificate https://download.docker.com/linux/debian/gpg -O - | apt-key add -
echo "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list

cat <<EOF > /etc/docker/daemon.json
{
  "storage-driver": "btrfs"
}
EOF

apt-get update

apt-get install docker-ce

# Register the runner using the register_runner.sh script (modify where needed)
# Add a cronjob using for example gitlab-runner_verify.sh script
